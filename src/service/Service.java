package service;

import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class Service {
    /***
     * Menentukan voucher dgn point terbesar
     * @param voucherMap data voucher
     * @return point terbesar
     */
    public Map<String, Integer> countHighestPoint(Map<Double, Integer> voucherMap){
        Map<String, Integer> res  = new HashMap<>();
        int maxValue = (Collections.max(voucherMap.values()));
        String key = null;
        for (Map.Entry<Double, Integer> entry : voucherMap.entrySet()) {
            if (entry.getValue().equals(maxValue)) {
                key = "Rp. " + entry.getKey().intValue();
                res.put(key, maxValue);
            }
        }
        return res;
    }

    /***
     * Menghitung sisa poin setelah di redeem dgn point terbesar 
     * @param currentPoint point saat ini
     * @param voucherMap data voucher
     * @return sisa point setelah di redeem dgn point terbesar
     */
    public int redeemWithHighestPoint(int currentPoint, Map<Double, Integer> voucherMap){
        int highestPoint = (Collections.max(voucherMap.values()));
        int pointLeft = currentPoint - highestPoint;
        return pointLeft;
    }

    /***
     * Redem all point dgn memprioritaskan poin terbesar
     * @param currentPoint point saat ini
     * @param voucherMap data voucher
     * @return Hasil dari setelah di redeem dengan point terbesar
     */
    public Map<String, Integer> redeemWithHighestPointPriority(int currentPoint, Map<Double, Integer> voucherMap){
        Map<String, Integer> res = new LinkedHashMap<>();
        List<Integer> highestPoint = voucherMap.values().stream().sorted(Collections.reverseOrder()).collect(Collectors.toList());
        List<Double> highestKey = voucherMap.keySet().stream().sorted(Collections.reverseOrder()).collect(Collectors.toList());
        for (int i = 0; i < highestPoint.size();) {
            if (currentPoint >= highestPoint.get(i)) {
                currentPoint = currentPoint - highestPoint.get(i);
                if (Boolean.FALSE.equals(res.containsKey("Rp. " + highestKey.get(i).intValue() + " "))) {
                    res.put("Rp. " + highestKey.get(i).intValue() + " ", 1);
                }else {
                    res.replace("Rp. " + highestKey.get(i).intValue() + " ", res.get("Rp. " + highestKey.get(i).intValue() + " ") + 1);
                }
            } else {
                i++;
            }
        }
        res.put("Sisa point ", currentPoint);
        return res;
    }
}