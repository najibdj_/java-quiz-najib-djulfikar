import java.util.HashMap;
import java.util.Map;

import service.Service;

public class App {

    public static void main(String[] args) throws Exception {
        Map<Double, Integer> voucherData = new HashMap<>();
        String line = "---------------------------------------------------------------------------------------------------";
        Service service = new Service();
        
        voucherData.put(10000D, 100);
        voucherData.put(25000D, 200);
        voucherData.put(50000D, 400);
        voucherData.put(100000D, 800);
        
        System.out.println(line);
        System.out.println("RESULT");
        System.out.println(line);
        // Menentukan voucher dgn point terbesar
        System.out.println("1. Voucher dengan point terbesar: ");
        service.countHighestPoint(voucherData).forEach((k, v) -> {
            System.out.println(k + " : " + v + "p");
        });
        System.out.println();
        
        // Menghitung sisa poin setelah di redeem dgn point terbesar jika poin yg dimilik adalah 1000p
        System.out.println("2. Sisa poin setelah di redeem dgn point terbesar jika poin yg dimilik adalah 1000p: ");
        System.out.println(service.redeemWithHighestPoint(1000, voucherData) + " point");
        System.out.println();
        
        // Redem all point dgn memprioritaskan poin terbesar. Contoh jika memiliki 1150p, maka akan mendapatkan voucher 100rb x1, 25rb x1 dan 10rb xq dgn sisa poin adalah 50p
        System.out.println("3. Voucher yang didapatkan dan sisa point jika poin yang dimiliki 1150p:");
        service.redeemWithHighestPointPriority(1150, voucherData).forEach((k, v) -> {
            System.out.print(k + " : ");
            System.out.println(v);
        });
        System.out.println(line);
    }

}
